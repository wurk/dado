﻿using System;
using System.Linq;
using FluentAssertions;
using LanguageExt;
using LanguageExt.Trans.Linq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using static Wurk.Dado.GitVersionInformation;

namespace Wurk.Dado.Tests {
    [TestFixture]
    [TestOf(typeof(GitVersionInformation))]
    public class TestGitVersioning {

        public static void IsAValidNumber( string versionData )
            => Assert.GreaterOrEqual( Convert.ToInt32( versionData ), 0 );

        [Test]
        public void TestMajor() => IsAValidNumber( Major );

        [Test]
        public void TestMinor() => IsAValidNumber( Minor );

        [Test]
        public void TestPatch() => IsAValidNumber( Patch );

//        [Test]
//        public void TestPreReleaseTag() => Assert.IsNotEmpty( PreReleaseTag );
//
//        [Test]
//        public void TestPreReleaseTagWithDash() => Assert.IsNotEmpty( PreReleaseTagWithDash );
//
//        [Test]
//        public void TestPreReleaseLabel() => Assert.IsNotEmpty( PreReleaseLabel );
//
//        [Test]
//        public void TestPreReleaseNumber() => IsAValidNumber( PreReleaseNumber );

        [Test]
        public void TestBuildMetaData() => IsAValidNumber(BuildMetaData);

        [Test]
        public void TestBuildMetaDataPadded() => IsAValidNumber(BuildMetaData);

        [Test]
        public void TestBuildMetaDataPaddedLength() => Assert.AreEqual( 4, BuildMetaDataPadded.Length );

        [Test]
        public void TestFullBuildMetaData() => Assert.IsNotEmpty( FullBuildMetaData );

        [Test]
        public void TestMajorMinorPatch()
            => Assert.AreEqual( 3, MajorMinorPatch.Split( '.' ).Map( Convert.ToInt32 ).Count() );

        [Test]
        public void TestSemVer() => Assert.IsNotEmpty( SemVer );





        /*      
                                                                                35
                                                                                         public static string SemVer = "0.4.0-add-tests.1";
                                                                                36
                                                                                         public static string LegacySemVer = "0.4.0-add-tests1";
                                                                                37
                                                                                         public static string LegacySemVerPadded = "0.4.0-add-tests0001";
                                                                                38
                                                                                         public static string AssemblySemVer = "0.4.0.0";
                                                                                39
                                                                                         public static string FullSemVer = "0.4.0-add-tests.1+12";
                                                                                40
                                                                                         public static string InformationalVersion = "0.4.0-add-tests.1+12.Branch.feature/add_tests.Sha.a2161ad524044d7a827b3a201421749bf775d428";
                                                                                41
                                                                                         public static string BranchName = "feature/add_tests";
                                                                                42
                                                                                         public static string Sha = "a2161ad524044d7a827b3a201421749bf775d428";
                                                                                43
                                                                                         public static string NuGetVersionV2 = "0.4.0-add-tests0001";
                                                                                44
                                                                                         public static string NuGetVersion = "0.4.0-add-tests0001";
                                                                                45
                                                                                         public static string CommitsSinceVersionSource = "12";
                                                                                46
                                                                                         public static string CommitsSinceVersionSourcePadded = "0012";
                                                                                47
                                                                                         public static string CommitDate = "2016-11-08";
                                                                                         */
    }
}