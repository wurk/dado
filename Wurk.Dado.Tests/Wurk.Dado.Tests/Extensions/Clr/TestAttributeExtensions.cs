﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Wurk.Dado.Extensions.Clr;
using Wurk.Dado.Guard;

namespace Wurk.Dado.Tests.Extensions.Clr {
    [TestFixture]
    [TestOf(typeof(AttributeExtensions))]
    public class TestAttributeExtensions
    {
        private AssemblyTitleAttribute titleAttribute;
        private Assembly assembly;

        [OneTimeSetUp]
        public void Setup() {
            assembly = typeof(Ensure).Assembly;
            titleAttribute = GetAttribute<AssemblyTitleAttribute>( assembly );
        }

        [Test]
        [Description( @"Tests our Attribute TryGetProperty extension method" )]
        public void TestTryGetProperty()
            => Assert.AreEqual( titleAttribute.Title, titleAttribute.TryGetProperty( attribute => attribute.Title ) );

        [Test]
        [Description( @"Tests our Attribute GetAttribute extension method" )]
        public void TestGetAttribute()
            => Assert.AreEqual( titleAttribute, assembly.GetAttribute<AssemblyTitleAttribute>() );

        /// <summary>
        ///     Gets the first attribute of type T from the specified assembly
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="attributeProvider">The assembly.</param>
        /// <returns></returns>
        private T GetAttribute<T>( ICustomAttributeProvider attributeProvider )
            where T : Attribute {
            return attributeProvider.GetCustomAttributes( typeof(T), false )
                                    .OfType<T>()
                                    .FirstOrDefault();
        }
    }
}