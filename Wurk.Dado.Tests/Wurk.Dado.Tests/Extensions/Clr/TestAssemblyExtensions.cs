﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Wurk.Dado.Extensions.Clr;
using Wurk.Dado.Guard;

namespace Wurk.Dado.Tests.Extensions.Clr {
    [TestFixture]
    [TestOf(typeof(AssemblyExtensions))]
    public class TestAssemblyExtensions {
        private AssemblyTitleAttribute titleAttribute;
        private Assembly assembly;
        private AssemblyDescriptionAttribute descriptionAttribute;

        [OneTimeSetUp]
        public void Setup() {
            assembly = typeof(Ensure).Assembly;
            titleAttribute = GetAttribute<AssemblyTitleAttribute>( assembly );
            descriptionAttribute = GetAttribute<AssemblyDescriptionAttribute>( assembly );
        }

        [Test]
        [Description( @"Tests our Assembly GetTitle extension method" )]
        public void TestGetTitle()
            => Assert.AreEqual( titleAttribute.Title, assembly.GetTitle() );

        [Test]
        [Description( @"Tests our Assembly GetDescription extension method" )]
        public void TestGetDescription()
            => Assert.AreEqual( descriptionAttribute.Description, assembly.GetDescription() );


        /// <summary>
        ///     Gets the first attribute of type T from the specified assembly
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="attributeProvider">The assembly.</param>
        /// <returns></returns>
        private T GetAttribute<T>( ICustomAttributeProvider attributeProvider )
            where T : Attribute {
            return attributeProvider.GetCustomAttributes( typeof(T), false )
                                    .OfType<T>()
                                    .FirstOrDefault();
        }
    }
}