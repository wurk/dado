﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Dado.Extensions.String;

namespace Wurk.Dado.Tests.Extensions {
    [TestFixture]
    [TestOf( typeof(StringExtensions) )]
    public class TestStringExtensions {

        [Test]
        [Description( "Tests the CoerceToIntOrNull extension method with a decimal string" )]
        public void TestCoerceToIntOrNullWithDecimal()
            => Assert.AreEqual( null, "123.123".CoerceToIntOrNull() );

        [Test]
        [Description( "Tests the CoerceToIntOrNull extension method with a numeric string" )]
        public void TestCoerceToIntOrNullWithInt()
            => Assert.AreEqual( 123, @"123".CoerceToIntOrNull() );

        [Test]
        [Description( "Tests the CoerceToIntOrNull extension method with a empty string" )]
        public void TestCoerceToIntOrNullWithNull()
            => Assert.AreEqual( null, "".CoerceToIntOrNull() );

        [Test]
        [Description( "Tests the CoerceToInt extension method with a whitespace string" )]
        public void TestCoerceToIntWithBlank()
            => Assert.AreEqual( 0, " ".CoerceToInt() );

        [Test]
        [Description( "Tests the CoerceToInt extension method with a decimal string" )]
        public void TestCoerceToIntWithDecimal()
            => new Action( () => "123.123".CoerceToInt() ).ShouldThrow<FormatException>();

        [Test]
        [Description( "Tests the CoerceToInt extension method with a numeric string" )]
        public void TestCoerceToIntWithInt()
            => Assert.AreEqual( 123, @"123".CoerceToInt() );

        [Test]
        [Description( "Tests the CoerceToInt extension method with a blank string" )]
        public void TestCoerceToIntWithNull()
            => Assert.AreEqual( 0, "".CoerceToInt() );

        [Test]
        [Description( @"Tests the CoerceToNullableBoolean extension method with blank" )]
        public void TestCoerceToNullableBooleanWithBlank()
            => Assert.AreEqual( null, string.Empty.CoerceToNullableBoolean() );

        [Test]
        [Description( @"Tests the CoerceToNullableBoolean extension method with custom false" )]
        public void TestCoerceToNullableBooleanWithCustomFalse()
            => Assert.AreEqual( false, @"N".CoerceToNullableBoolean( @"Y", @"N" ) );

        [Test]
        [Description( @"Tests the CoerceToNullableBoolean extension method with custom true" )]
        public void TestCoerceToNullableBooleanWithCustomTrue()
            => Assert.AreEqual( true, @"Y".CoerceToNullableBoolean( @"Y", @"N" ) );

        [Test]
        [Description( @"Tests the CoerceToNullableBoolean extension method with false" )]
        public void TestCoerceToNullableBooleanWithFalse()
            => Assert.AreEqual( false, @"false".CoerceToNullableBoolean() );

        [Test]
        [Description( @"Tests the CoerceToNullableBoolean extension method with null" )]
        public void TestCoerceToNullableBooleanWithNull()
            => Assert.AreEqual( null, ((string) null).CoerceToNullableBoolean() );

        [Test]
        [Description( @"Tests the CoerceToNullableBoolean extension method with true" )]
        public void TestCoerceToNullableBooleanWithTrue()
            => Assert.AreEqual( true, @"true".CoerceToNullableBoolean() );

        [Test]
        [Description( @"Tests the CoerceToNullableBoolean extension method with whitespace" )]
        public void TestCoerceToNullableBooleanWithWhitespace()
            => Assert.AreEqual( null, @" ".CoerceToNullableBoolean() );

        [Test]
        [Description( @"Tests the CoerceToNullableInt extension method with blank" )]
        public void TestCoerceToNullableIntWithBlank()
            => Assert.AreEqual( null, string.Empty.CoerceToNullableInt() );

        [Test]
        [Description( @"Tests the CoerceToNullableInt extension method default case" )]
        public void TestCoerceToNullableIntWithInt()
            => Assert.AreEqual( 3, "3".CoerceToNullableInt() );

        [Test]
        [Description( @"Tests the CoerceToNullableInt extension method with null" )]
        public void TestCoerceToNullableIntWithNull()
            => Assert.AreEqual( null, ((string) null).CoerceToNullableInt() );

        [Test]
        [Description( @"Tests the HasEnd extension method success case" )]
        public void TestHasEnd()
            => Assert.AreEqual( true, nameof( StringExtensions.HasEnd ).HasEnd( "End" ) );

        [Test]
        [Description( @"Tests the HasEnd extension method fails" )]
        public void TestHasEndFails()
            => Assert.AreEqual( false, nameof( StringExtensions.HasEnd ).HasEnd( "NoMatch" ) );

        [Test]
        [Description( @"Tests the HasEnd extension method with null" )]
        public void TestHasEndWithNull()
            => Assert.AreEqual( false, nameof( StringExtensions.HasEnd ).HasEnd( null ) );

        [Test]
        [Description( @"Tests the HasStart extension method success case" )]
        public void TestHasStart()
            => Assert.AreEqual( true, nameof( StringExtensions.HasStart ).HasStart( "Has" ) );

        [Test]
        [Description( @"Tests the HasStart extension method fails" )]
        public void TestHasStartFails()
            => Assert.AreEqual( false, nameof( StringExtensions.HasStart ).HasStart( "NoMatch" ) );

        [Test]
        [Description( @"Tests the HasStart extension method with null" )]
        public void TestHasStartWithNull()
            => Assert.AreEqual( false, nameof( StringExtensions.HasStart ).HasStart( null ) );

        [Test]
        [Description( @"Tests the HasValue extension method failure case" )]
        public void TestHasValue()
            => Assert.AreEqual( false, string.Empty.HasValue() );

        [Test]
        [Description( @"Tests the HasValue extension method success case" )]
        public void TestHasValueFails()
            => Assert.AreEqual( true, @" asdf ".HasValue() );

        [Test]
        [Description( @"Tests the HasValue extension method with null" )]
        public void TestHasValueWithNull()
            => Assert.AreEqual( false, ((string) null).HasValue() );

        [Test]
        [Description( @"Tests the HasValue extension method with whitespace" )]
        public void TestHasValueWithWhitespace()
            => Assert.AreEqual( false, @"   ".HasValue() );

        [Test]
        [Description( @"Tests the IsBlank extension method success case" )]
        public void TestIsBlank()
            => Assert.AreEqual( true, @"".IsBlank() );

        [Test]
        [Description( @"Tests the IsBlank extension method failure case" )]
        public void TestIsBlankFails()
            => Assert.AreEqual( false, @" asdf ".IsBlank() );

        [Test]
        [Description( @"Tests the IsBlank extension method with null" )]
        public void TestIsBlankWithNull()
            => Assert.AreEqual( true, ((string) null).IsBlank() );

        [Test]
        [Description( @"Tests the IsBlank extension method with whitespace" )]
        public void TestIsBlankWithWhitespace()
            => Assert.AreEqual( false, @"   ".IsBlank() );

        [Test]
        [Description( @"Tests the Redact extension method" )]
        public void TestRedact()
            => Assert.AreEqual( @"!@#$1234%^&*()_+1234", @"asdf!@#$1234%^&*asdf()_+1234".Redact( @"asdf" ) );

        [Test]
        [Description( @"Tests the RedactFirst extension method" )]
        public void TestRedactFirst()
            => Assert.AreEqual( @"asdf!@#$%^&*asdf()_+1234", @"asdf!@#$1234%^&*asdf()_+1234".RedactFirst( @"1234" ) );

        [Test]
        [Description( @"Tests the RedactLast extension method" )]
        public void TestRedactLast()
            => Assert.AreEqual( @"asdf!@#$1234%^&*()_+1234", @"asdf!@#$1234%^&*asdf()_+1234".RedactLast( @"asdf" ) );

        [Test]
        [Description( @"Tests the TitleSpaced extension method" )]
        public void TestTitleSpaced()
            => Assert.AreEqual( @"This Is A Title A", @"ThisIsATitleA".TitleSpaced() );

        [Test]
        [Description( @"Tests the ToBoolean extension method with blank" )]
        public void TestToBooleanWithBlank()
            => Assert.AreEqual( false, string.Empty.ToBoolean() );

        [Test]
        [Description( @"Tests the ToBoolean extension method with custom false" )]
        public void TestToBooleanWithCustomFalse()
            => Assert.AreEqual( false, @"N".ToBoolean( @"Y", @"N" ) );

        [Test]
        [Description( @"Tests the ToBoolean extension method with custom true" )]
        public void TestToBooleanWithCustomTrue()
            => Assert.AreEqual( true, @"Y".ToBoolean( @"Y", @"N" ) );

        [Test]
        [Description( @"Tests the ToBoolean extension method with false" )]
        public void TestToBooleanWithFalse()
            => Assert.AreEqual( false, @"false".ToBoolean() );

        [Test]
        [Description( @"Tests the ToBoolean extension method with null" )]
        public void TestToBooleanWithNull()
            => Assert.AreEqual( false, ((string) null).ToBoolean() );

        [Test]
        [Description( @"Tests the ToBoolean extension method with true" )]
        public void TestToBooleanWithTrue()
            => Assert.AreEqual( true, @"true".ToBoolean() );

        [Test]
        [Description( @"Tests the ToBoolean extension method with whitespace" )]
        public void TestToBooleanWithWhitespace()
            => Assert.AreEqual( false, @" ".ToBoolean() );

        [Test]
        [Description( @"Tests the ToDictionary extension method with a valid collection" )]
        public void TestToDictionary()
            =>
            "key=value, key2=value2".ToDictionary()
                                    .ShouldBeEquivalentTo( new Dictionary<string, string> {
                                        {"key", "value"},
                                        {"key2", "value2"}
                                    } );

        [Test]
        [Description( @"Tests the CoerceToNullableInt extension method with whitespace" )]
        public void TestToNullableIntWithWhitespace()
            => Assert.AreEqual( null, @" ".CoerceToNullableInt() );

        [Test]
        [Description( @"Tests the ToShortName method" )]
        public void TestToShortName()
            => Assert.AreEqual( @"ShortName", @"Short Name".ToShortName() );

        [Test]
        [Description( @"Tests the Truncate extension method with a long string" )]
        public void TestTruncateLongString()
            => Assert.AreEqual( @"JustLong ...", "JustLongEnough".Truncate( 12 ) );

        [Test]
        [Description(@"Tests the Truncate extension method with a short enough string")]
        public void TestTruncateShortString()
            => Assert.AreEqual(@"JustLongEnough", "JustLongEnough".Truncate(14));

        [Test]
        [Description( @"Tests the Limit extension method with a long string" )]
        public void TestLimitLongString()
            => Assert.AreEqual( @"JustLongEnou", "JustLongEnough".Limit( 12 ) );

        [Test]
        [Description( @"Tests the Limit extension method with a short enough string" )]
        public void TestLimitShortString()
            => Assert.AreEqual( @"JustLongEnough", "JustLongEnough".Limit( 14 ) );

        [Test]
        [Description( @"Tests the ValueOrEmpty extension method with a null string" )]
        public void TestValueOrEmpty()
            => Assert.AreEqual( string.Empty, ((string) null).ValueOrEmpty() );


    }
}