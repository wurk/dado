using System;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Dado.Guard;

namespace Wurk.Dado.Tests.Guard {
    [TestFixture]
    [TestOf( typeof(Ensure) )]
    public partial class TestEnsure {

        [Test]
        [Description( @"Tests the Instantiated object extension method with an instantiated value" )]
        public void TestEnsureInstantiatedPasses() {
            object someParameter = "someParameter";
            new Action( () => Ensure.Instantiated( someParameter, nameof( someParameter ) ) )
                .ShouldNotThrow("the object is instantiated");
        }

        [Test]
        [Description( @"Tests the Instantiated object extension method with a null value" )]
        public void TestEnsureInstantiatedThrows() {
            object someParameter = null;
            new Action( () => Ensure.Instantiated( someParameter, nameof( someParameter ) ) )
                .ShouldThrow<ArgumentNullException>("the object is null");
        }
    }
}