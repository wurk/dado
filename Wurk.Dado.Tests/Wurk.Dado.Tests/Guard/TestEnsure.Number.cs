using System;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Dado.Guard;

namespace Wurk.Dado.Tests.Guard {
    public partial class TestEnsure {

        [TestFixture]
        [TestOf( typeof(Ensure.Number) )]
        public class Number {

            private static readonly string testParameter = @"testParameter";

            [Test]
            [Description( @"Tests the Maximum number guard method with an equivalent value" )]
            public void MaxEqualToPasses() =>
                new Action( () => Ensure.Number.Maximum( 5, 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the number is small enough" );

            [Test]
            [Description( @"Tests the Maximum number guard method with a insufficient value" )]
            public void MaxGreaterThanThrows() =>
                new Action( () => Ensure.Number.Maximum( 6, 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentOutOfRangeException>( "because the number is too large" );

            [Test]
            [Description( @"Tests the Maximum number guard method with a lesser value" )]
            public void MaxLessThanPasses() =>
                new Action( () => Ensure.Number.Maximum( 4, 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the number is small enough" );

            [Test]
            [Description( @"Tests the Maximum number guard method with a null value" )]
            public void MaxNullThrows() =>
                new Action( () => Ensure.Number.Maximum( null, 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the number is small enough" );

            [Test]
            [Description( @"Tests the Minimum number guard method with an equivalent value" )]
            public void MinEqualToPasses() =>
                new Action( () => Ensure.Number.Minimum( 5, 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the number is large enough" );

            [Test]
            [Description( @"Tests the Minimum number guard method with a insufficient value" )]
            public void MinGreaterThanPasses() =>
                new Action( () => Ensure.Number.Minimum( 6, 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the number is large enough" );

            [Test]
            [Description( @"Tests the Minimum number guard method with a insufficient value" )]
            public void MinLessThanThrows() =>
                new Action( () => Ensure.Number.Minimum( 4, 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentOutOfRangeException>( "because the number is too small" );

            [Test]
            [Description( @"Tests the Minimum number guard method with a null value" )]
            public void MinNullThrows() =>
                new Action( () => Ensure.Number.Minimum( null, 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentNullException>( "because the number is null" );

            [Test]
            [Description( @"Tests the Range number guard method with a maximum value for the range" )]
            public void RangeEqualToMaxPasses() =>
                new Action( () => Ensure.Number.Range( 9, 5, 9, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the number is the maximum for the range" );

            [Test]
            [Description( @"Tests the Range number guard method with a minimum value for the range" )]
            public void RangeEqualToMinPasses() =>
                new Action( () => Ensure.Number.Range( 5, 5, 9, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the number is the minimum for the range" );

            [Test]
            [Description( @"Tests the Range number guard method with a value that exceeds the range" )]
            public void RangeGreaterThanThrows() =>
                new Action( () => Ensure.Number.Range( 12, 5, 9, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentOutOfRangeException>( "because the number is too large" );

            [Test]
            [Description( @"Tests the Range number guard method with a value that exceeds the range" )]
            public void RangeLessThanThrows() =>
                new Action( () => Ensure.Number.Range( 3, 5, 9, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentOutOfRangeException>( "because the number is too small" );

            [Test]
            [Description( @"Tests the Range number guard method with a null value" )]
            public void RangeNullThrows() =>
                new Action( () => Ensure.Number.Range( null, 5, 9, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentNullException>( "because the number is null" );
        }
    }
}