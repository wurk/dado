using System;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Dado.Guard;

namespace Wurk.Dado.Tests.Guard {
    /// <summary>
    ///     Tests our <see cref="Ensure" /> contract guard methods.
    /// </summary>
    public partial class TestEnsure {

        /// <summary>
        ///     Tests our <see cref="Ensure.String" /> contract guard methods.
        /// </summary>
        [TestFixture]
        [TestOf( typeof(Ensure.Enumerable) )]
        public class Enumerable {

            [Test]
            [Description( @"Tests the HasItems enumerable guard method with an empty value" )]
            public void HasItemsWithEmpty()
                => new Action( () => Ensure.Enumerable.HasItems( new object[] {}, @"testParameter" ) )
                    .ShouldThrow<ArgumentException>( "the collection is empty" );

            [Test]
            [Description( @"Tests the HasItems enumerable guard method with items" )]
            public void HasItemsWithItems()
                => new Action( () => Ensure.Enumerable.HasItems( new[] {1, 2, 3}, @"testParameter" ) )
                    .ShouldNotThrow( "the collection has items" );

            [Test]
            [Description( @"Tests the HasItems enumerable guard method with a null value" )]
            public void HasItemsWithNull()
                => new Action( () => Ensure.Enumerable.HasItems( (object[]) null, @"testParameter" ) )
                    .ShouldThrow<ArgumentNullException>( "the collection is null" );
        }
    }
}