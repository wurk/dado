using System;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Dado.Guard;

namespace Wurk.Dado.Tests.Guard {
    public partial class TestEnsure {

        [TestFixture]
        [TestOf( typeof(Ensure.String) )]
        public class String {
            private static readonly string testParameter = @"testParameter";

            [Test]
            [Description( @"Tests the ExactLength string guard method with a null value" )]
            public void ExactLengthNullThrows() =>
                new Action( () => Ensure.String.ExactLength( null, 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentNullException>( "the string is null" );

            [Test]
            [Description( @"Tests the ExactLength string guard method with an acceptable value" )]
            public void ExactLengthPasses() =>
                new Action( () => Ensure.String.ExactLength( @"asdfa", 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "the string is the exact length" );

            [Test]
            [Description( @"Tests the ExactLength string guard method with an unacceptable value" )]
            public void ExactLengthThrows() =>
                new Action( () => Ensure.String.ExactLength( @"asdf", 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentOutOfRangeException>( "the string is too short" );

            [Test]
            [Description( @"Tests the HasValue string guard method with a not null value" )]
            public void HasValuePasses() =>
                new Action( () => Ensure.String.HasValue( @"something", nameof( testParameter ) ) )
                    .ShouldNotThrow( "the string has a value" );

            [Test]
            [Description( @"Tests the HasValue string guard method with a whitespace value" )]
            public void HasValuePassesWhitespace() =>
                new Action( () => Ensure.String.HasValue( @"  ", nameof( testParameter ) ) )
                    .ShouldNotThrow( "the string has a value" );

            [Test]
            [Description( @"Tests the HasValue string guard method with a null value" )]
            public void HasValueThrows() =>
                new Action( () => Ensure.String.HasValue( null, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentNullException>( "the string is null" );

            [Test]
            [Description( @"Tests the HasValue string guard method with a blank value" )]
            public void HasValueThrowsWithBlank() =>
                new Action( () => Ensure.String.HasValue( string.Empty, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentException>( "the string is empty" );

            [Test]
            [Description( @"Tests the MaxLength string guard method with a null value" )]
            public void MaxLengthNullPasses() =>
                new Action( () => Ensure.String.MaxLength( null, 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "because the string is null" );

            [Test]
            [Description( @"Tests the MaxLength string guard method with an acceptable value" )]
            public void MaxLengthPasses() =>
                new Action( () => Ensure.String.MaxLength( @"asdfa", 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "the string is long enough" );

            [Test]
            [Description( @"Tests the MaxLength string guard method with an unacceptable value" )]
            public void MaxLengthThrows() =>
                new Action( () => Ensure.String.MaxLength( @"asdfad", 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentOutOfRangeException>( "the string is too long" );

            [Test]
            [Description( @"Tests the MinLength string guard method with a null value" )]
            public void MinLengthNullThrows() =>
                new Action( () => Ensure.String.MinLength( null, 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentNullException>( "the string is null" );

            [Test]
            [Description( @"Tests the MinLength string guard method with an acceptable value" )]
            public void MinLengthPasses() =>
                new Action( () => Ensure.String.MinLength( @"asdfa", 5, nameof( testParameter ) ) )
                    .ShouldNotThrow( "the string is long enough" );

            [Test]
            [Description( @"Tests the MinLength string guard method with an unacceptable value" )]
            public void MinLengthThrows() =>
                new Action( () => Ensure.String.MinLength( @"asdf", 5, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentOutOfRangeException>( "the string is too short" );

            [Test]
            [Description( @"Tests the NotBlank string guard method with a not null value" )]
            public void NotBlankPasses() =>
                new Action( () => Ensure.String.NotBlank( @"something", nameof( testParameter ) ) )
                    .ShouldNotThrow( "the string has a value" );

            [Test]
            [Description( @"Tests the NotBlank string guard method with a null value" )]
            public void NotBlankThrows() =>
                new Action( () => Ensure.String.NotBlank( null, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentNullException>( "the string is null" );

            [Test]
            [Description( @"Tests the NotBlank string guard method with a whitespace value" )]
            public void NotBlankThrowsWhitespace() =>
                new Action( () => Ensure.String.NotBlank( @"  ", nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentException>( "the string is whitespace" );

            [Test]
            [Description( @"Tests the NotBlank string guard method with a blank value" )]
            public void NotBlankThrowsWithBlank() =>
                new Action( () => Ensure.String.NotBlank( string.Empty, nameof( testParameter ) ) )
                    .ShouldThrow<ArgumentException>( "the string is empty" );
        }
    }
}