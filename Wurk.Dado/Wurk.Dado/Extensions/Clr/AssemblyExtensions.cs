﻿using System;
using System.Linq;
using System.Reflection;

namespace Wurk.Dado.Extensions.Clr {
    /// <summary>
    ///     Some generic utility extension methods for `Assembly` metadata
    /// </summary>
    public static class AssemblyExtensions {
       

        /// <summary>
        /// Gets the title value from the `AssemblyTitle` attribute if available
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static string GetTitle( this Assembly assembly )
            => assembly.GetAttribute<AssemblyTitleAttribute>()?.Title;

        /// <summary>
        /// Gets the description value from the `AssemblyDescription` attribute if available
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static string GetDescription( this Assembly assembly )
            => assembly.GetAttribute<AssemblyDescriptionAttribute>()?.Description;
    }

}