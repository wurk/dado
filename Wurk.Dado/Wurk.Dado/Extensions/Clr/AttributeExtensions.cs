﻿using System;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;

namespace Wurk.Dado.Extensions.Clr {
    /// <summary>
    ///     Extension methods for `Attribute` metadata
    /// </summary>
    [PublicAPI]
    public static class AttributeExtensions {
        /// <summary>
        ///     Tries to get the property value.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <typeparam name="TReturn">The type of the return.</typeparam>
        /// <param name="attribute">The attribute.</param>
        /// <param name="accessor">The accessor.</param>
        /// <returns></returns>
        public static TReturn TryGetProperty<TAttribute, TReturn>( this TAttribute attribute,
                                                                   Func<TAttribute, TReturn> accessor )
            where TAttribute : Attribute where TReturn : class
        => attribute == null ? default(TReturn) : accessor?.Invoke( attribute );

        /// <summary>
        ///     Gets the first attribute of type T from the specified <paramref name="attributeProvider"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="attributeProvider">The attributeProvider.</param>
        /// <returns></returns>
        public static T GetAttribute<T>( this ICustomAttributeProvider attributeProvider )
            where T : Attribute => attributeProvider.GetCustomAttributes( typeof(T), false )
                                                    .OfType<T>()
                                                    .FirstOrDefault();
    }
}