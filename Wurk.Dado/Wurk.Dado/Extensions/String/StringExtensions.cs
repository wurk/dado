﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Wurk.Dado.Guard;

namespace Wurk.Dado.Extensions.String {
    /// <summary>
    ///     A <see langword="static" /> utility class providing extension methods
    ///     for strings.
    /// </summary>
    public static class StringExtensions {

        /// <summary>
        ///     Removes every instance of the specified substring from a string
        /// </summary>
        /// <param name="target"></param>
        /// <param name="redact"></param>
        /// <returns>
        ///     The redacted string
        /// </returns>
        public static string Redact( this string target, string redact )
            => target.Replace( redact, string.Empty );

        /// <summary>
        ///     Places spaces prior to upper case letters, useful for converting
        ///     Enums and other strings to display forms
        /// </summary>
        /// <param name="target">The string to title-case</param>
        /// <returns>
        ///     The title-spaced string
        /// </returns>
        public static string TitleSpaced( this string target )
            => target.Aggregate( string.Empty, ( acc, c ) =>
                         acc +
                         (char.IsUpper( c ) ? $" " : string.Empty) + c )
                     .Trim( null );

        /// <summary>
        ///     Determines if a string is blank or <c>null</c>
        /// </summary>
        /// <param name="target">The string to check</param>
        /// <returns>
        ///     <c>true</c> if there is a blank string or <c>null</c> in
        ///     <paramref name="target" /> , otherwise, <c>false</c> .
        /// </returns>
        public static bool IsBlank( this string target )
            => string.IsNullOrEmpty( target );

        /// <summary>
        ///     Determines if a string is solely whitespace, or <c>null</c>
        /// </summary>
        /// <param name="target">The string to check</param>
        /// <returns>
        ///     <c>true</c> if there is solely whitespace or <c>null</c> in
        ///     <paramref name="target" /> , otherwise, <c>false</c> .
        /// </returns>
        public static bool IsBlankOrWhitespace( this string target )
            => string.IsNullOrEmpty( target ) || !target.Trim().Any();


        /// <summary>
        ///     Determines if a string possesses any non-whitespace value
        /// </summary>
        /// <param name="target">The string to check</param>
        /// <returns>
        ///     <c>true</c> if any value exists in <paramref name="target" /> ,
        ///     otherwise, <c>false</c> .
        /// </returns>
        public static bool HasValue( this string target )
            => !target.IsBlankOrWhitespace();

        /// <summary>
        ///     Determines whether the targeted string starts with the specified
        ///     string.
        /// </summary>
        /// <param name="target">The string to evaluate for matches.</param>
        /// <param name="substring">
        ///     The <paramref name="substring" /> to look for.
        /// </param>
        /// <param name="stringComparison">The StringComparison to use for the operation</param>
        /// <returns>
        ///     <c>true</c> if the specified <paramref name="substring" /> is found;
        ///     otherwise, <c>false</c> .
        /// </returns>
        public static bool HasStart( this string target, string substring, StringComparison stringComparison = StringComparison.Ordinal)
            => substring != null && target.StartsWith( substring, stringComparison );

        /// <summary>
        ///     Determines whether the targeted string ends with the specified
        ///     string.
        /// </summary>
        /// <param name="target">The string to evaluate for matches.</param>
        /// <param name="substring">The substring to look for.</param>
        /// <param name="stringComparison">The StringComparison to use for the operation</param>
        /// <returns>
        ///     <c>true</c> if the specified <paramref name="substring" /> is found;
        ///     otherwise, <c>false</c> .
        /// </returns>
        public static bool HasEnd( this string target, string substring, StringComparison stringComparison = StringComparison.Ordinal)
            => substring != null && target.EndsWith( substring, stringComparison );


        /// <summary>
        ///     Removes the first instance of the specified substring from a string
        /// </summary>
        /// <param name="target">The string to evaluate for matches.</param>
        /// <param name="redact">The substring to redact.</param>
        /// <param name="stringComparison">The StringComparison to use for the operation</param>
        /// <returns>
        ///     The redacted string
        /// </returns>
        public static string RedactFirst( this string target, string redact, StringComparison stringComparison = StringComparison.Ordinal)
            => target.IndexOf( redact, stringComparison ) < 0
                ? target
                : target.Remove( target.IndexOf( redact, stringComparison ), redact.Length );


        /// <summary>
        ///     Removes the last instance of the specified substring from a string
        /// </summary>
        /// <param name="target">The string to evaluate for matches.</param>
        /// <param name="redact">The substring to redact.</param>
        /// <param name="stringComparison">The StringComparison to use for the operation</param>
        /// <returns>
        ///     The redacted string
        /// </returns>
        public static string RedactLast( this string target, string redact, StringComparison stringComparison = StringComparison.Ordinal)
            => target.LastIndexOf( redact, stringComparison ) < 0
                ? target
                : target.Remove( target.LastIndexOf( redact, stringComparison ), redact.Length );

        /// <summary>
        /// Converts the passed string into a boolean
        /// </summary>
        /// <param name="target">The string to convert.</param>
        /// <param name="falseText"></param>
        /// <param name="trueText"></param>
        /// <returns></returns>
        public static bool ToBoolean( this string target, string trueText = "true", string falseText = "false" )
            => target.CoerceToNullableBoolean( trueText, falseText ).GetValueOrDefault( false );

        /// <summary>
        /// </summary>
        /// <param name="number"></param>
        /// <returns>
        /// </returns>
        public static int CoerceToInt( this string number )
            => number.IsBlankOrWhitespace() ? 0 : Convert.ToInt32( number, CultureInfo.InvariantCulture );

        /// <summary>
        /// </summary>
        /// <param name="number"></param>
        /// <returns>
        /// </returns>
        public static int? CoerceToIntOrNull( this string number ) {
            int result;
            return !int.TryParse( number, NumberStyles.Integer, CultureInfo.InvariantCulture, out result )
                ? (int?) null
                : result;
        }

        /// <summary>
        ///     Splits a string containing a comma separated list of key-value pairs
        ///     into a dictionary.
        /// </summary>
        /// <param name="target">
        ///     Input string, e.g. "key1=value1,key2=value2
        /// </param>
        /// <returns>
        ///     A dictionary of key/value pairs
        /// </returns>
        public static Dictionary<string, string> ToDictionary( this string target ) {

            return target.Split( ',' )
                         .Select( s => s.Trim() )
                         .ToDictionary( s => s.Split( '=' )?[0]?.Trim(), s => s.Split( '=' )?[1]?.Trim() );
        }

        /// <summary>
        /// </summary>
        /// <param name="value"></param>
        /// <param name="trueText"></param>
        /// <param name="falseText"></param>
        /// <returns>
        /// </returns>
        public static bool? CoerceToNullableBoolean( this string value, string trueText = @"true", string falseText = @"false" ) {
            Ensure.String.NotBlank( trueText, nameof( trueText ) );
            Ensure.String.NotBlank( falseText, nameof( falseText ) );

            if ( value.IsBlankOrWhitespace() )
                return null;
            if ( value == falseText )
                return false;
            return (value == trueText) || bool.Parse( value );
        }

        /// <summary>
        /// </summary>
        /// <param name="value"></param>
        /// <returns>
        /// </returns>
        public static int? CoerceToNullableInt( this string value )
            => value.IsBlankOrWhitespace() ? (int?) null : Convert.ToInt32( value );

        /// <summary>
        /// </summary>
        /// <param name="value"></param>
        /// <returns>
        /// </returns>
        public static string ToTitleCase( this string value ) {
            var textInfo = new CultureInfo( "en-US", false ).TextInfo;
            return textInfo.ToTitleCase( value );
        }

        /// <summary>
        /// </summary>
        /// <param name="target"></param>
        /// <param name="length"></param>
        /// <returns>
        /// </returns>
        public static string Limit( this string target, int length )
            => (target == null) || (target.Length <= length) ? target : target.Substring( 0, length );

        /// <summary>
        /// </summary>
        /// <param name="target"></param>
        /// <param name="length"></param>
        /// <param name="marker"></param>
        /// <returns>
        /// </returns>
        public static string Truncate( this string target, int length, string marker = @" ..." )
            =>
            (target == null) || (target.Length <= length)
                ? target
                : target.Substring( 0, length - marker.Length ) + marker;

        /// <summary>
        /// </summary>
        /// <param name="value"></param>
        /// <returns>
        /// </returns>
        public static string ValueOrEmpty( this string value ) {
            return value ?? string.Empty;
        }

        /// <summary>
        ///     To a short name.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// </returns>
        public static string ToShortName( this string input ) => input?.ToTitleCase().Redact( " " );
    }
}