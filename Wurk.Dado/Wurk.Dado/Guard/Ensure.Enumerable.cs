﻿using System;
using System.Collections;
using System.Linq;
using JetBrains.Annotations;

namespace Wurk.Dado.Guard {

    /// <inheritdoc />
    public static partial class Ensure {


        /// <summary>
        ///     Our method contract guards for Enumerable types
        /// </summary>
        [PublicAPI]
        public static class Enumerable {
            /// <summary>
            ///     Ensures the given <paramref name="sequence" /> is not
            ///     <see langword="null" /> or empty.
            /// </summary>
            /// <param name="sequence">The sequence.</param>
            /// <param name="parameterName">Name of the parameter.</param>
            /// <exception cref="ArgumentNullException" />
            /// <exception cref="ArgumentOutOfRangeException" />
            public static void HasItems<TEnumerable>(
                [AssertionCondition( AssertionConditionType.IS_NOT_NULL )] TEnumerable sequence,
                string parameterName ) where TEnumerable : class, IEnumerable {

                Instantiated( sequence, parameterName );

                if ( (sequence as object[] ?? sequence.Cast<object>()).Any() )
                    return;
                Fail.Parameter.IsEmpty<TEnumerable>( parameterName );

            }
        }
    }
}