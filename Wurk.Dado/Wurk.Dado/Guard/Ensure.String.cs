﻿using System;
using JetBrains.Annotations;
using Wurk.Dado.Extensions.String;

namespace Wurk.Dado.Guard {

    /// <inheritdoc />
    public static partial class Ensure {


        public static class String {
            /// <summary>
            ///     Ensures the given <paramref name="argument" /> is not
            ///     <see langword="null" />
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentNullException">{paramName}</exception>
            /// <exception cref="ArgumentOutOfRangeException" />
            [AssertionMethod]
            public static void HasValue(
                [AssertionCondition(AssertionConditionType.IS_NOT_NULL)] [NoEnumeration] string argument,
                [InvokerParameterName, NotNull] string paramName ) {

                Instantiated( argument, paramName );
                if ( !argument.IsBlank() )
                    return;
                Fail.Parameter.IsEmpty<string>( paramName );
            }

            /// <summary>
            ///     Ensures the given <paramref name="argument" /> is not
            ///     <see langword="null" /> or blank.
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentNullException">{paramName}</exception>
            /// <exception cref="ArgumentOutOfRangeException" />
            [AssertionMethod]
            public static void NotBlank( [AssertionCondition( AssertionConditionType.IS_NOT_NULL )] string argument,
                                         string paramName ) {
                Instantiated( argument, paramName );
                if ( !argument.IsBlankOrWhitespace() )
                    return;
                Fail.Parameter.IsEmpty<string>( paramName );
            }


            /// <summary>
            ///     Ensures the given <paramref name="argument" /> is shorter than
            ///     the specified length. Throws
            ///     <see cref="ArgumentOutOfRangeException" /> otherwise
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="length">The length.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentOutOfRangeException" />
            public static void MaxLength( string argument, int length, string paramName ) {
                if ( argument == null || argument.Length <= length )
                    return;
                Fail.Parameter.ExceedsBounds<string>( paramName, $@"less than {length} in length" );
            }

            /// <summary>
            ///     Ensures the given <paramref name="argument" /> is shorter than
            ///     the specified length. Throws
            ///     <see cref="ArgumentOutOfRangeException" /> otherwise
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="length">The length.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentOutOfRangeException" />
            public static void MinLength( string argument, int length, string paramName ) {
                Instantiated(argument, paramName);
                if ( argument.Length >= length )
                    return;
                Fail.Parameter.ExceedsBounds<string>( paramName, $@"greater than {length} in length" );
            }

            /// <summary>
            ///     Ensures the given <paramref name="argument" /> is exactly the
            ///     specified length. Throws
            ///     <see cref="ArgumentOutOfRangeException" /> otherwise.
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="length">The length.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentOutOfRangeException" />
            public static void ExactLength( string argument, int length, string paramName ) {
                Instantiated( argument, paramName );
                if ( argument?.Length == length )
                    return;
                Fail.Parameter.ExceedsBounds<string>( paramName, $@"exactly {length} in length" );
            }
        }
    }
}