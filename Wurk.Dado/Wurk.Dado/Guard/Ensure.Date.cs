﻿using JetBrains.Annotations;

namespace Wurk.Dado.Guard {

    /// <inheritdoc />
    public static partial class Ensure {


        /// <summary>
        ///     Our method contract guards for Date types
        /// </summary>
        [PublicAPI]
        public static class Date {
            /* TODO:
             * 
             *   .IsZulu / AreZulu
             *   .IsPast
             *   .IsFuture
             *   .Before
             *   .After
             *   .Between
             *   .HasLocale
             *   .Contemporary [1850-2150]
             *   
             */
        }
    }
}