﻿using System;
using System.Diagnostics;
using JetBrains.Annotations;

namespace Wurk.Dado.Guard {
    /// <summary>
    ///     A <see langword="static" /> utility class to provide guard clauses
    ///     within your own code.
    /// </summary>
    /// <para>
    ///     This provides us with a series of utility methods:
    ///     (each method here provides both a named parameter version, and a
    ///     version that extracts the name via lambda capture
    ///     - Instantiated() : object, string, and enumerable versions
    ///     HasItems() : IEnumerable
    ///     Has() :
    /// </para>
    [DebuggerStepThrough]
    [DebuggerNonUserCode]
    [PublicAPI]
    public static partial class Ensure {
        /* 
         * TODO:         
         * Ensure
         *   .Each( set, nameof(set), x => x.SomeProperty );
         *   .EachInstantiated<T>( set, nameof(set) )
         *   .Is<T>()
         *   .HasInterface<T>()
         *   .HasSupertype<T>()
         */

        /// <summary>
        ///     Ensures the given <paramref name="argumentValue" /> is not null.
        ///     Throws <see cref="ArgumentNullException" /> otherwise.
        /// </summary>
        /// <example>
        ///     Ensure.Instantiated(myParam, nameof(myParam))
        /// </example>
        /// <param name="argumentValue">The argument value.</param>
        /// <param name="paramName">Name of the parameter.</param>
        /// <exception cref="ArgumentNullException" />
        [ContractAnnotation( "argumentValue:null => halt" )]
        [AssertionMethod]
        [DebuggerHidden]
        public static void Instantiated<T>(
            [AssertionCondition( AssertionConditionType.IS_NOT_NULL )] [NoEnumeration] T argumentValue,
            [InvokerParameterName] [NotNull] string paramName ) {

            if ( argumentValue == null )
                Fail.Parameter.IsNull<T>( paramName );
        }
    }
}