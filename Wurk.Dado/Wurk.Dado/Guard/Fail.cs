﻿using System;

namespace Wurk.Dado.Guard {
    /// <summary>
    /// Provides utility classes for exceptions for use by the Ensure contract clauses
    /// </summary>
    public static class Fail {
        public static class Parameter {

            /// <summary>
            /// Determines whether the specified parameter name is null.
            /// </summary>
            /// <typeparam name="TParam">The type of the parameter.</typeparam>
            /// <param name="paramName">Name of the parameter.</param>
            /// <param name="hasRequirement">The has requirement.</param>
            /// <param name="failure">The failure.</param>
            public static void IsNull<TParam>( string paramName, string hasRequirement = @"is instantiated",
                                               string failure = @"was null" )
                => FailParameter<ArgumentNullException, TParam>( paramName, hasRequirement, failure );

            public static void IsEmpty<TParam>( string paramName, string hasRequirement = @"has a value",
                                                string failure = @"was empty" )
                => FailParameter<ArgumentException, TParam>( paramName, hasRequirement, failure );

            public static void ExceedsBounds<TParam>( string paramName, string hasRequirement = @"within declared range",
                                                      string failure = @"exceeded declared range" )
                => FailParameter<ArgumentOutOfRangeException, TParam>( paramName, hasRequirement, failure );

            public static void FailParameter<TException, TParam>( string parameterName, string hasRequirement,
                                                                  string failure )
                where TException : System.ArgumentException, new() {
                throw (TException)
                    Activator.CreateInstance( typeof(TException),
                        $@"The method contract requires that the {typeof(TParam).Name} {parameterName} {hasRequirement}. The passed {typeof
                            (TParam).Name} {failure}",
                        parameterName );
            }
        }
    }
}