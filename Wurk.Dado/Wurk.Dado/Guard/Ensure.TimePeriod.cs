﻿using System;
using System.Collections;
using System.Linq;
using JetBrains.Annotations;

namespace Wurk.Dado.Guard {

    /// <inheritdoc />
    public static partial class Ensure {


        /// <summary>
        ///     Our method contract guards for TimePeriods
        /// </summary>
        [PublicAPI]
        public static class TimePeriod {
            /* TODO:
             *   .Before( DateTime beforeDate, DateTimePeriod myRange )
             *   .After
             *   .During
             *   .StartsBefore
             *   .StartsAfter
             *   .StartsDuring
             *   .EndsBefore
             *   .EndsAfter
             *   .EndsDuring
             *   .Overlaps
             *   
             */
        }
    }
}