﻿using System;
using System.Collections;
using System.Linq;
using JetBrains.Annotations;

namespace Wurk.Dado.Guard {

    /// <inheritdoc />
    public static partial class Ensure {


        /// <summary>
        ///     Our method contract guards for the system Host
        /// </summary>
        [PublicAPI]
        public static class Host {
        }
    }
}