﻿using System;
using JetBrains.Annotations;

namespace Wurk.Dado.Guard {

    /// <inheritdoc />
    public static partial class Ensure {


        /// <summary>
        ///     Our method contract guards for Number types
        /// </summary>
        [PublicAPI]
        public static class Number {
            /// <summary>
            ///     Ensures the given <paramref name="argument" /> is at least the
            ///     specified value. Throws
            ///     <see cref="ArgumentOutOfRangeException" /> otherwise
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="minValue">The minimum value.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentOutOfRangeException" />
            public static void Minimum( int? argument, int minValue, string paramName ) {
                Instantiated( argument, paramName );
                if ( argument < minValue )
                    Fail.Parameter.ExceedsBounds<int>( paramName, $"greater or equal to {minValue}",
                            $"less than {minValue}" );
            }

            /// <summary>
            ///     Ensures the given <paramref name="argument" /> is less than the
            ///     specified value. Throws
            ///     <see cref="ArgumentOutOfRangeException" /> otherwise
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="maxValue">The maximum value.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentOutOfRangeException">
            ///     The maximum value for {paramName} is {maxValue}")
            /// </exception>
            public static void Maximum( int? argument, int maxValue, string paramName ) {
                if ( !argument.HasValue || (argument <= maxValue) )
                    return;
                Fail.Parameter.ExceedsBounds<int>(paramName, $"less than or equal to {maxValue}", $"greater than {maxValue}");
            }

            /// <summary>
            ///     Ensures the given <paramref name="argument" /> falls withing the
            ///     specified range. Throws
            ///     <see cref="ArgumentOutOfRangeException" /> otherwise
            /// </summary>
            /// <param name="argument">The argument.</param>
            /// <param name="minValue">The minimum value.</param>
            /// <param name="maxValue">The maximum value.</param>
            /// <param name="paramName">Name of the parameter.</param>
            /// <exception cref="ArgumentOutOfRangeException">
            ///     The allowed range for {paramName} is {minValue} to {maxValue}");
            /// </exception>
            public static void Range( int? argument, int minValue, int maxValue, string paramName ) {
                Minimum( argument, minValue, paramName );
                Maximum( argument, maxValue, paramName );
            }
        }
    }
}