﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle( "Dado" )]
[assembly: AssemblyDescription("Some .net niceties, and simple fundamental utilities")]
[assembly: AssemblyCompany( "Wurk" )]
[assembly: AssemblyProduct( "Dado" )]
[assembly: AssemblyCopyright( "Copyright - Wurk, 2016" )]
[assembly: AssemblyCulture( "" )]

[assembly: ComVisible( false )]
[assembly: Guid( "a1d22b31-fe1e-4c8c-a734-600232b3c884" )]
[assembly: InternalsVisibleTo("Wurk.Dado.Tests")]



// Our versioning is provided by Anvil